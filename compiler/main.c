#include "compiler.h"
#include "helpers.h"
#include "style.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

void init(sdc_io* io, int argc, char** argv) {
	if (argc < 2 || argc > 3) {
		puts("E: You must provide a document to compile and one optional mode");
		exit(SDC_BADARGS);
	}
	char* docname;
	char* mode = NULL;
	if (argc == 3) {
		mode = argv[1];
		docname = argv[2];
	} else {
		docname = argv[1];
	}
	
	io->line = 1;
	io->col  = 0;

	/// Set up the IO streams
	if (!(io->input = fopen(docname, "r"))) {
		printf("E: Could not open file '%s' for reading\n", docname);
		exit(SDC_IOFAILURE);
	}
	size_t inputlen = strlen(docname)+1;
	char* title_base = malloc(inputlen*2 + strlen(".html"));
	char* title_output = title_base + inputlen;
	sscanf(docname, "%[^.]", title_base);
	sprintf(title_output, "%s.html", title_base);
	if (!(io->output = fopen(title_output, "w"))) {
		printf("E: Could not open file '%s' for writing\n", title_output);
		exit(SDC_IOFAILURE);
	}
	
	/// Get the current time to a buffer for the document timestamp
	time_t rawtime;
	time(&rawtime);
	struct tm* timeinfo = localtime(&rawtime);
	char timestr[11];
	strftime(&timestr[0], 11, "%F", timeinfo);
	
	/// Add the document title and timestamp to the HTML start block
	size_t startsize;
	FILE* ms = open_memstream(&io->start, &startsize);
	fprintf(ms, D_START, &title_base[0]);
	if (mode) {
		if (strcmp(mode, "wiki") == 0) {
			fprintf(ms, D_WIKI, &title_base[0], &timestr[0]);
		} else if (strcmp(mode, "paper") == 0) {
			//TODO: do something here
		} //TODO: more modes
	}
	fclose(ms);
}


int main(int argc, char** argv) {
	sdc_io io;
	init(&io, argc, argv);
	swrite(&io, io.start);
	compile(&io, CTX_DOCUMENT);
	swrite(&io, "\n</body></html>");
	free(io.start);
	fclose(io.input);
	fclose(io.output);
}

