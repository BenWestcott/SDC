#define D_START "<!DOCTYPE html><html><head>\n\
<meta charset=\"utf-8\">\n\
<title>%s</title>\n\
<style>\n\
	body{text-align:justify}\n\
	h1{margin:0; text-align:center}\n\
	hr.h1{margin-top:-5pt; margin-bottom:15pt}\n\
	h3.subtitle{margin-top:-12pt; margin-left:15pt; margin-bottom:25pt;\n\
		font-style:oblique}\n\
	h2,h3,h4{margin:0}\n\
	p{margin-bottom:2pt}\n\
	ol,ul{margin-top:2pt; margin-bottom:2pt; text-align:start}\n\
	ol>ol{list-style:lower-alpha}\n\
	ol>ol>ol{list-style:lower-roman}\n\
	ol>ol>ol>ol{list-style:lower-greek}\n\
	ol>ol>ol>ol>ol>ol{list-style:lower-alpha}\n\
	ol>ol>ol>ol>ol>ol>ol{list-style:lower-roman}\n\
	ol>ol>ol>ol>ol>ol>ol>ol{list-style:lower-greek}\n\
	div.pbr{height:20pt}\n\
	span.indent{display:inline-block; width:25pt}\n\
	.center{display:inline-block; position:relative; left:50%%; transform:translateX(-50%%);}\n\
	pre{margin:0}\n\
	.code{background-color:lightgray; font-family:monospace; border-radius:4pt;\n\
		padding-left:2pt;padding-right:2pt; padding-top:1pt;\n\
		padding-bottom:1pt}\n\
	div.code{border-left: 3pt solid dimgrey; margin-left:20pt; margin-right:10pt;\n\
		width:max-content; padding-left:5pt; padding-right:3pt}\n\
	.blockquote{border-left: 2pt solid black; margin-left:20pt; padding-left:8pt;\n\
		margin-right:10pt;}\n\
	.blockquoteauthor{width:90%%; text-align:right}\n\
	details{border:2pt solid lightgray; border-radius:10pt; overflow:hidden; margin-bottom:5pt}\n\
	summary{padding-left:5pt; padding-top:2pt; font-size:15pt; font-weight:bold}\n\
	summary:hover,summary:focus{background-color:lightgray}\n\
	summary:active{background-color:darkgray}\n\
	div.section{padding:5pt}\n\
	table{border-collapse:collapse; margin-bottom:5pt; text-align:start}\n\
	table.narrow{width:min(100%%,270pt)}\n\
	table.wide{width:80%%}\n\
	table.fullwidth{width:100%%}\n\
	table.center{display:inline-table}\n\
	table.sidebar-left{float:left; margin-right:15pt}\n\
	table.sidebar-right{float:right; margin-left:15pt}\n\
	table.noborder{border: none !important}\n\
	td.table,td.header{border:1pt solid black; padding-left:4pt; padding-right:4pt;\n\
		padding-top:3pt; padding-bottom:2pt}\n\
	td.header{font-weight:bold; text-align:center; background-color:lightgray}\n\
	figure{margin:5pt; text-align:center; font-style:oblique}\n\
	img{object-fit:contain}\n\
	a{text-decoration-line:inherit; color:darkblue}\n\
	a:hover{color:blue}\n\
	a:active{color:blueviolet}\n\
	a::after{display:inline-block; rotate:-45deg;\n\
		content:url(\"data:image/svg+xml,%%3Csvg xmlns='http://www.w3.org/2000/svg' width='21' height='18'%%3E%%3Cpath d='M19.188 12.001c0 1.1-.891 2.015-1.988 2.015l-4.195-.015c.538 1.088.963 1.999 1.997 1.999h3c1.656 0 2.998-2.343 2.998-4s-1.342-4-2.998-4h-3c-1.034 0-1.459.911-1.998 1.999l4.195-.015c1.098 0 1.989.917 1.989 2.017z'/%%3E%%3Cpath d='M8 12c0 .535.42 1 .938 1h6.109c.518 0 .938-.465.938-1 0-.534-.42-1-.938-1h-6.109c-.518 0-.938.466-.938 1z'/%%3E%%3Cpath d='M4.816 11.999c0-1.1.891-2.015 1.988-2.015l4.196.015c-.539-1.088-.964-1.999-1.998-1.999h-3c-1.656 0-2.998 2.343-2.998 4s1.342 4 2.998 4h3c1.034 0 1.459-.911 1.998-1.999l-4.195.015c-1.098 0-1.989-.917-1.989-2.017z'/%%3E%%3C/svg%%3E\")}\n\
	a.noicon::after{content:none}\n\
</style>\n\
</head><body>\n"

#define D_WIKI "\
	<table style=\"height:20pt; width:100%%; background-color:lightgrey;\n\
		border-radius:5pt; margin-bottom:13pt\"><tr>\n\
	<td style=\"width:15%%;text-align:left;padding-left:5pt\">\n\
		<a class=noicon href=\".\">&lt;-</a></td>\n\
	<td style=\"width:70%%;text-align:center;padding-top:2.5pt\">%s</td>\n\
	<td style=\"width:15%%;text-align:right;padding-right:5pt;\n\
		padding-top:2.4pt\">%s</td>\n\
</tr></table>"

#define D_PAPER ""
