#!/bin/sh
cd $(dirname $0)
cc=$(which clang 2>/dev/null || which gcc)
warn='-Wall -Wextra --pedantic'
ignore='-Wno-implicit-fallthrough'
other='-O0 -g -fstack-protector-strong'
$cc helpers.c compiler.c main.c -o sdc $warn $ignore $other

