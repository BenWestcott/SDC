# SDC
Simple Document Compiler. Bridges the gap between Markdown and LaTeX.  
SDE converts a custom document language into ultra-lightweight HTML.

The compiler has been rewritten in C due to the contextual limitations of
regular expression substitutions.

## Current Status
See 'implemented.md' for a list of current features, and 'todo.md' for a list
of bugs and planned features.

'Feature Test.sd' attempts to demonstrate and test all SD features, to assist
with development. It is naturally rather ugly.  
'Example Document.sd' shows an example of what a normal document built with
the current feature set might look like.

